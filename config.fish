functions --copy cd cd_without_ls

if status is-interactive
    stty intr ^u
    function cd
        cd_without_ls $argv[1]
        and eza --icons -a
        and cd_history add
    end
    function rm
        echo use `seriously-rm`
    end
    # rbenv init - fish | source
end

function prompt_login --description 'Print a description of the user and host suitable for the prompt'
    if not set -q __fish_machine
        set -g __fish_machine
        set -l debian_chroot $debian_chroot

        if test -r /etc/debian_chroot
            set debian_chroot (cat /etc/debian_chroot)
        end

        if set -q debian_chroot[1]
            and test -n "$debian_chroot"
            set -g __fish_machine "(chroot:$debian_chroot)"
        end
    end

    # Prepend the chroot environment if present
    if set -q __fish_machine[1]
        echo -n -s (set_color yellow) "$__fish_machine" (set_color normal) ' '
    end

    # If we're running via SSH, change the host color.
    set -l color_host $fish_color_host
    if set -q SSH_TTY; and set -q fish_color_host_remote
        set color_host $fish_color_host_remote
    end

    echo -n -s (set_color $fish_color_user) "$USER" (set_color $fish_color_param) @ (set_color $color_host) (prompt_hostname) (set_color normal)
end

function fish_prompt --description 'Write out the prompt'
    set -l last_pipestatus $pipestatus
    set -lx __fish_last_status $status # Export for __fish_print_pipestatus.
    set -l normal (set_color normal)
    set -q fish_color_status
    or set -g fish_color_status --background=red white
    set color_host 999

    # Color the prompt differently when we're root
    set -l color_cwd $fish_color_cwd
    set -l suffix '\n->'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix '#'
    end

    # Write pipestatus
    # If the status was carried over (e.g. after `set`), don't bold it.
    set -l bold_flag --bold
    set -q __fish_prompt_status_generation; or set -g __fish_prompt_status_generation $status_generation
    if test $__fish_prompt_status_generation = $status_generation
        set bold_flag
    end
    set __fish_prompt_status_generation $status_generation
    set -l status_color (set_color $fish_color_status)
    set -l statusb_color (set_color $bold_flag $fish_color_status)
    set -l prompt_status (__fish_print_pipestatus "[" "]" "|" "$status_color" "$statusb_color" $last_pipestatus)

    echo -n -s -e (prompt_login)' ' (set_color $color_cwd) (prompt_pwd) $normal (fish_vcs_prompt) $normal " "$prompt_status (set_color 999) $suffix " " $normal
end

function prompt2
    set p (prompt_pwd)
    echo -n (set_color 999);
    echo -ns $p " "
    string repeat -n (echo "$COLUMNS - $(string length $p) - 1" | bc) -;
    echo -n '->' (set_color normal)
end

function watch-modify
    inotifywait -me close_write $argv[1] | while command read
        eval $argv[2]
    end
end

function mkcd
    mkdir $argv[1]
    cd $argv[1]
end

function latex-paren
    ruby -pne \
        '$_.gsub!(/(?<!\\\\left)\\(/,"\\\\left(");
        $_.gsub!(/(?<!\\\\right)\\)/,"\\\\right)");
        $_.gsub!(/(?<!\\\\left)\\\\{/,"\\\\left\\\\{");
        $_.gsub!(/(?<!\\\\right)\\\\}/,"\\\\right\\\\}");'
end

function nihongo-space
    ruby -pne '$_.gsub!(/([^ -~[:punct:]])([0-9]*[!-\'*-\\/:-~][0-9]*)/,"\\\\1 \\\\2");$_.gsub!(/([0-9]*[!-\'*-\\/:-~][0-9]*)([^ -~[:punct:]])/,"\\\\1 \\\\2")'
end

function nibb
    cd_without_ls ~/works/old/nibbles
    echo $argv[1] > Main.nbl
    ./nibbles -hs Main.nbl
    and ~/.ghcup/bin/ghcup --offline run --quick --ghc 8.10.7 -- ghc -O2 out.hs
    and ./out
    cd_without_ls -
end

function s
    history --merge
    set c (history | fzf +s -e)
    and commandline $c
end

function j
    set c (cd_history list | fzf +s -e)
    and cd $c
end

# opam
# source /home/mado/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
# fenv source "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"
# direnv hook fish | source
# set -x EDITOR /usr/bin/code

alias ls='ls --color'
alias ll='eza -aal --icons'
alias solunm='fusermount3 -u ~/solm'
alias cbcopy='xsel --clipboard --input'
alias cbpaste='xsel --clipboard --output'
alias seriously-rm='command rm'

abbr -a brightness 'xrandr --output eDP --brightness'
abbr -a solp 'ssh sol -ND 1080'
abbr -a kutoten 'sed -i -e "s/。\$/./g" -e "s/、\$/,/g" -e "s/。/. /g" -e "s/、/, /g"'
abbr -a latex-paren 'ruby -pne \'$_.gsub!(/(?<!\\\\\\\\left)\\(/,"\\\\\\\\left(");$_.gsub!(/(?<!\\\\\\\\right)\\)/,"\\\\\\\\right)")\''
abbr -a latex-brack 'ruby -pne \'$_.gsub!(/(?<!\\\\\\\\left)\\[/,"\\\\\\\\left[");$_.gsub!(/(?<!\\\\\\\\right)\\]/,"\\\\\\\\right]")\''
abbr -a dl "cd ~/dl"
abbr -a rc 'code --new-window ~/.config/fish/config.fish'
abbr -a mmaunm 'fusermount3 -u ~/mmam'
abbr -a screenoff 'sleep 1; xset dpms force off'
abbr -a mi 'micro'
abbr -a c 'code .'
abbr -a gcc11 'gcc -Wall -std=c11'
abbr -a clangir 'clang -std=c17 -S -emit-llvm -fno-unroll-loops'
abbr -a clang17 'clang -std=c17 -fsanitize=undefined -fno-sanitize-recover -ftrivial-auto-var-init=pattern'
abbr -a cursor-b 'gsettings set org.gnome.desktop.interface cursor-size 64'
abbr -a cursor-s 'gsettings set org.gnome.desktop.interface cursor-size 24'
abbr -a cursor-set 'gsettings set org.gnome.desktop.interface cursor-size'
abbr -a sync-cotton 'rsync -avh . cot:works/cotton --exclude target --exclude flamegraph.svg --exclude perf.data'
abbr -a en-noc 'env NO_COLOR=1'
abbr -a en-trace 'env RUST_BACKTRACE=1'
abbr -a trace 'env RUST_BACKTRACE=1'
abbr -a fd 'doki r --cc tcc -C=-w'
abbr -a sync-momostr 'rsync -avchu ~/works/momostr komugi:/home/ubuntu --exclude target --delete'
abbr -a lab 'ssh -A kiban -t fish'
abbr -a komugi 'ssh komugi -t fish'
abbr --set-cursor -a aj 'get_activity % | jq'

# opam configuration
# source /home/mado/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
